.NOTPARALLEL:
.PHONY: default

default: docker-build docker-run

workdir = $(shell pwd)

docker-build:
	docker-compose build

docker-run:
	docker-compose run --rm ansible-gitlab bash

docker-pull:
	docker-compose pull

docker-push:
	docker-compose push
